# INCLUSIVE

Inclusive é uma iniciativa colaborativa que tem como objetivo a popularização do software livre e das redes comunitárias como meios para viabilizar o acesso a Ambientes Virtuais de Aprendizagem, especialmente em comunidades com escassez de recursos computacionais e/ou de conectividade. Tais redes autônomas comunitárias devem poder operar de forma independente da Internet, serem construídas a partir de componentes abertos que estejam alinhados com a filosofia do software livre e geridas pelas próprias comunidades nas quais estejam operando.

<!-- ![alt text](imgs/inclusive.jpg) -->
<div align="center">
<img src="imgs/inclusive.jpg" alt="Inclusive" width="640"/>
</div>


* Informações sobre redes comunitárias e algumas das propostas iniciais do INCLUSIVE podem ser encontradas na nossa cartilha:

    * http://libreroom.org/inclusive/cartilha.pdf

<br/>

* Informações sobre o desenvolvimento do projeto durante duas edições dos [Laboratórios de Emergência](https://labdeemergencia.silo.org.br) promovidos pela [Silo – Arte e Latitude Rural](http://silo.org.br) podem ser encontradas em:

    * https://labdeemergencia.silo.org.br/2ed/pt/ambientes-virtuais/

    * https://labdeemergencia.silo.org.br/3ed/pt/mutiroes/





# Cenário 1

- Em um primeiro cenário são sugeridos modelos específicos de equipamentos no papel de servidor (computador de placa única RaspberryPi 4) e de roteador (Linksys WRT54G)

<!-- ![alt text](imgs/rasp.jpeg) ![alt text](imgs/linksys.jpeg) -->
<div align="center">
<img src="imgs/rasp.jpeg" alt="Inclusive" width="480" align="center"/>
<img src="imgs/linksys.jpeg" alt="Inclusive" width="420" align="center"/>
</div>

<!-- Baixe a imagem do Raspberry Pi OS disponível no endereço ()

* No windows:  

* No Ubuntu Linux:  
-->
  

# Cenário 2

- Um segundo cenário trata da possibilidade de se adaptar dispositivos já disponíveis na comunidade (como computadores e aparelhos de rede antigos) para que possam desempenhar as funções de servidores e roteadores.
<div align="center">
<img src="imgs/old-tech-comp-phone.jpg" alt="Desktop as server" width="480" align="center"/>
</div>

<!-- > sudo systemctl enable ssh -->
<!-- > sudo systemctl start ssh -->
<!-- > sudo apt install apache2 -y -->


# Componentes e referências externas:

- Moodle: https://moodle.org/
- Docker Hub: https://hub.docker.com/
- Moodle Docker (ARM): https://hub.docker.com/r/treehouses/moodle
- NextCloud Docker Image (ARM): https://hub.docker.com/r/arm32v7/nextcloud
- OnlyOffice Docker Image (ARM):
- Synapse Matrix Client Docker Image (ARM): https://hub.docker.com/r/black0/synapse
- Mautrix - Matrix Bridge for Whatsapp: https://github.com/tulir/mautrix-whatsapp/wiki/Bridge-setup-with-Docker
- OnlyOffice: https://github.com/ONLYOFFICE/

